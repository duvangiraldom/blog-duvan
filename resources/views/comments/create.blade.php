@extends('layouts.app')

@section('content')
    <h1>Create Comment</h1>
    {!! Form::open(['action' => 'CommentsController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', '',['id'=>'article-ckeditor', 'class'=>'form-control','placeholder'=>'Body Text'])}}
        </div>
        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
