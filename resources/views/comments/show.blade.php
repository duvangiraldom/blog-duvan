@extends('layouts.app')

@section('content')
    <a href="/comments" class="btn btn-default">Go Back</a>
    <div>
        {!!$comment->body!!}
    </div>
    <hr><small>Written on {{$comment->created_at}}</small>
    <a href="/comments/{{$comment->id}}/edit" class="btn btn-default">Edit</a>
    
    {!!Form::open(['action' => ['CommentsController@destroy', $comment->id],'method'=>'POST', 'class'=>'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class'=>'btn btn-danger'])}}
    {!!Form::close()!!}
@endsection
