@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-default">Go Back</a>
    <h1>{{$post->title}} </h1>
    <div>
        {!!$post->body!!}
    </div>
    <hr><small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
    <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a>
    
    {!!Form::open(['action' => ['PostsController@destroy', $post->id],'method'=>'POST', 'class'=>'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class'=>'btn btn-danger'])}}
    {!!Form::close()!!}


    <h1>Comments</h1>
    <p>-</p>
    @if(count($post->comments) > 0)
        @foreach($post->comments as $comment)
            <div class="well">
                <a>{!!$comment->body!!}</a>
            <small>Written on {{$comment->created_at}} by {{$comment->user->name}}</small>
            </div>
            <p>-</p>

        @endforeach
    @else
        <p>No comments found</p>
    @endif

    {!! Form::open(['action' => ['PostsController@addComment', $post->id], 'method' => 'POST']) !!}

        <div class="form-group">
            {{Form::textarea('body','',['id'=>'article-ckeditor', 'class'=>'form-control','placeholder'=>'Body Text'])}}
        </div>
        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}

@endsection
