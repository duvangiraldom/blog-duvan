<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Se pueden realizar los llamados de esta forma:
Route::get('/', function () {
    //return view('welcome');
    //Return Hello Wold text
    return "Hello World";
});

Route::get('/about', function () {
    //Return a view about
    return view('about');
});

Route::get('/user/{id}/{name}', function ($id,$name) {
    return 'Hello '.$name." with id: ".$id;
});
*/
// Pero la forma adecuada de realizar el return es creando un controller y
/* llamando un metodo de este asi:
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');

Route::get('/user/{id}/{name}', 'PagesController@user');

*/
Route::get('/', function () {
    return "Hello World";
});

Route::resource('posts','PostsController');
Route::resource('comments','CommentsController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

Route::get('/', function () {
    return view('welcome');
});

Route::post('posts/{id}', 'PostsController@addComment');
